<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$phones = array(
    "1111 1111" => "dima",
    "2222 2222" => "alex",
    "3333 3333" => "sem",
    "4444 4444" => "dima"
);
$app = new \Slim\App();
$app->add(new Tuupola\Middleware\HttpBasicAuthentication([
    "users" => [
        "root" => "admin",
        "somebody" => "password"
    ]
]));
//$app->config(array(debug=>"TRUE"));
$app->get('/phones/{name}', function (Request $request, Response $response, array $args) use ($app) {
    global $phones;
    $name = $args['name'];
    foreach (array_keys($phones, $name, true) as $key) {
     $res[] = array($name=>$phones[] = $key);
    }
     if($res){
       $response->getBody()->write(json_encode($res));
       return $response;  
     }
     else {
       //  
     }
       
    
});
$app->run();

